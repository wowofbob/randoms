# Bell's Test implementation #

* This program generates quantum circuit to run CHSH game on two normal bits.  
  Two bits win the game if their result is a pair of two equal bits.

* In simulation, this circuit is used to run game over list of pairs of bits.

* If in total there are approximately 85% pairs of bits won the game, then  
  given list was indeed random.

The [Quipper](http://www.mathstat.dal.ca/~selinger/quipper/) DSL language was used.
