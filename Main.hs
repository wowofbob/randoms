module Main (experiment, perform_experiment) where 

import Quipper

import Quipper.Monad
import Quipper.Generic

import RotGates
import Basis

measure_with gate b q = do
  q <- gate b q
  x <- measure q
  return x
  
alice's_gate :: ControlSource c => c -> Qubit -> Circ Qubit
alice's_gate b q = 
  gate_omega q `controlled` b

alice's_bit :: ControlSource c => c -> Qubit -> Circ Bit
alice's_bit b q =
  measure_with alice's_gate b q
  
bob's_gate :: Bit -> Qubit -> Circ Qubit
bob's_gate b q = do
  q <- gate_m_pi8 q `controlled` b
  b <- cnot b
  q <- gate_pi8 q `controlled` b
  return q

bob's_bit :: Bit -> Qubit -> Circ Bit
bob's_bit b q =
  measure_with bob's_gate b q

experiment (ab, aq) (bb, bq) = do
  ar <- alice's_bit ab aq
  br <- bob's_bit bb bq
  return (ar, br)

perform_experiment ab bb = do
  ab <- cinit_bit ab
  bb <- cinit_bit bb
  (aq, bq) <- bell_plus
  res <- experiment (ab, aq) (bb, bq)
  cdiscard (ab, bb)
  return res

      
