module Simulate where

import Quipper
import QuipperLib.Simulation

import Main
import Basis

import System.Random
import Control.Monad

import Data.List

print_experiment :: Bool -> Bool -> IO ()
print_experiment a b = print_simple Preview (perform_experiment a b)

-- Annotation needs to be here to
-- determine alice's_bit's ControlSOurce
perform :: (Bit, Bit) -> Circ (Bit, Bit)
perform (ab, bb) = do
    (aq, bq) <- bell_plus
    experiment (ab, aq) (bb, bq)

run_experiment :: (Bool, Bool) -> IO (Bool, Bool)
run_experiment = run_generic_io (undefined :: Double) perform
    
test_bits :: [(Bool, Bool)] -> IO [(Bool, Bool)]
test_bits = mapM run_experiment

rand_bits :: [(Bool, Bool)]
rand_bits = [(False, False), (True, False), (False, True)]
