module RotGates
( gate_pi8
, gate_m_pi8
) where

import Quipper

{-
  rot_gate1 =
    |   1       0     |
    |   0    exp(i*a) |

  gen_rot1  =
    | exp(i*a)    0    |
    |    0    exp(i*a) |
-}

rot1to2 :: (Qubit -> Circ Qubit) -> Qubit -> Circ Qubit
rot1to2 rot_gate1 q = do
  q <- rot_gate1  q
  q <- gate_X q
  q <- rot_gate1  q
  q <- gate_X q
  return q

{-
  gate_pi8 =
    | exp(i*pi/8)      0      |
    |     0       exp(i*pi/8) |
-}

gate_pi8 :: Qubit -> Circ Qubit
gate_pi8 = rot1to2 (rGate 4)

{-
  
  Rotates on pi, then pi/2, pi/4, pi/8
  because simple call of (rGate n q)
  where n < 0 throws an excaption "negative exponent"

  
  gate_m_pi8 =
    | exp(-i*pi/8)      0      |
    |     0       exp(-i*pi/8) |

-}
gate_m_pi8 :: Qubit -> Circ Qubit
gate_m_pi8 = rot1to2 rot15pi8
  where
    rot15pi8 q =  do
      q <- rGate 1 q
      q <- rGate 2 q
      q <- rGate 3 q
      q <- rGate 4 q
      return q
