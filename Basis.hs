module Basis
( plus_minus  
, bell
, bell_plus
) where

import Quipper

plus_minus :: Bool -> Circ Qubit
plus_minus b = do
  q <- qinit b
  q <- hadamard q
  return q
  
bell :: Bool -> Bool -> Circ (Qubit, Qubit)  
bell a b = do
  a <- plus_minus a
  b <- qinit b
  b <- qnot b `controlled` a
  return (a, b)

bell_plus :: Circ (Qubit, Qubit)
bell_plus = bell False False